import { Button, Table } from 'react-bootstrap'
import { add, list } from 'cart-localstorage'

import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'




export default function ViewCart(){

const cart = list();
console.log(cart.length)

for (let i=0; i < cart.length; i++){

	return(

		<>
			<div className="text-center my-4">
				<h2>Your Shopping Cart</h2>
			</div>

				<Table striped bordered hover responsive>
					<thead className="bg-dark text-white text-center">
						<tr>
							<th>Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr clas>
							<th className="col-5">{cart[i].name}</th>
							<th className="col-2"><span>&#8369;</span>{cart[i].price}</th>
							<th className="col-2">{cart[i].quantity}</th>
							<th className="col-3"><span>&#8369;</span>{cart[i].quantity * cart[i].price}</th>
							<th></th>
						</tr>
					</tbody>
				</Table>
		</>


	)
}
}