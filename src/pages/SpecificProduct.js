import { add } from 'cart-localstorage'
import { useState, useEffect, useContext } from 'react'
import { Link, useParams, useHistory } from 'react-router-dom'
import { InputGroup, FormControl, Row, Col, Card, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function SpecificProduct(){


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [quantity, setQuantity] = useState(1);



	const handleDecrement = () => {
		if(quantity > 1){
		setQuantity(prevCount => prevCount - 1)
		}
	}

	const handleIncrement = () => {
		if(quantity < 30){
		setQuantity(prevCount => prevCount + 1)
		}
	}

	const submitAddtoCart = (event) => {
		event.preventDefault();

		add({id: productId, name: name, price: price}, quantity)
		 
		Swal.fire({
			title: "Item added",
			icon: "success",
			text: "Item has been added to cart"
		})

		history.push("/products")

	} 

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const history = useHistory();

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	return(
		<Card className="my-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{name}</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>{description}</Card.Text>
				<Card.Text>Price: <span>&#8369;</span>{price}</Card.Text>
				{ user.id !== null ?
					<Row>
						<Col className="col-3">
							 <InputGroup className="mb-3">
							    <Button type="button" className="input-group-text" onClick={handleDecrement}>-</Button>
							    <FormControl className="text-center"value={quantity}/>
							    <Button type="button" className="input-group-text" onClick={handleIncrement}>+</Button>
							 </InputGroup>
						 </Col>
					 </Row>
					:
					<Row>
						<Col className="col-3">
							 <InputGroup className="mb-3">
							    <Button type="button" className="input-group-text disabled" >-</Button>
							    <FormControl className="text-center"value={quantity} min="1" max="50" />
							    <Button type="button" className="input-group-text disabled">+</Button>
							 </InputGroup>
						 </Col>
					 </Row>
					
				}
			</Card.Body>
			<Card.Footer className="d-grid gap-2">
				{ user.id !== null ?
					<Link className="btn btn-primary" to="#" onClick={submitAddtoCart}>Add to Cart</Link>
					:
					<Link className="btn btn-danger" to="/login">Log in to Add to Cart</Link>
				}
			</Card.Footer>
		</Card>

	)
	
}